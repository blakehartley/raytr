#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <iostream>
#include <fstream>
using namespace std;

#include "3Dtransfer.h"

typedef unsigned int uint;
typedef unsigned char uchar;

#define IMUL(a, b) __mul24(a, b)

#define MAX_EPSILON_ERROR 5.00f
#define THRESHOLD         0.30f

#define block_x 32

//double h=0.6711;
//double h0=h*3.246753e-18;
//double omegam=0.3;
//double yrtos=3.15569e7;
//time = time*yrtos*1.e6;
#define REDSHIFT_CONV (3.*0.6711*3.246753e-18*pow(0.3,0.5)/2.)*(3.15569e7*1.e6)

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "./3Dtransfer_kernels.cu"
    
// GLOBAL VARIABLES FOR DISPLAY
int numThreads1;
dim3 numBlocks1; 
int elements; 
__device__
float* DensArray_dev; 
__device__
float* x_HIArray_dev;
__device__
float* FluxArray_dev; 
__device__
short* FluxArray_flag_dev; 

cudaExtent size;
__device__
source* particles_dev;

__device__
int* dtnew;

cudaEvent_t timeA, timeB, timeC, timeD;
float time0, time1, time2;

// Error handling macro
#define CUDA_CHECK(call) \
    if((call) != cudaSuccess) { \
        cudaError_t err = cudaGetLastError(); \
        cerr << "CUDA error calling \""#call"\", code is " << err << endl; \
        my_abort(err); }

void rad(	float* DensArray, float* x_HIArray, source* particles, float* FluxArray,
			short* FluxArray_flag, int numParts, float L, float a)
{
	int elements = DIMX*DIMY*DIMZ;
	size_t size1=elements*sizeof(short);
	size_t size2=elements*sizeof(float);
	size = make_cudaExtent(DIMX, DIMY, DIMZ);
	
	size_t size3	= MAX_PARTICLES*sizeof(source);
	size_t size3f	= MAX_PARTICLES*sizeof(float);
	size_t numLoops	= numParts/MAX_PARTICLES;
	size_t remParts	= (numParts%MAX_PARTICLES)*sizeof(source);
	size_t remPartsf= (numParts%MAX_PARTICLES)*sizeof(float);
	
	numThreads1=MAX_PARTICLES;
	numBlocks1.x=DIMX;
	numBlocks1.y=DIMY;
	numBlocks1.z=DIMZ;
	//	printf("%d %d %d\n", numBlocks1.x,numBlocks1.y, numThreads1);
	
	// Perform calculations to produce Flux arrays
	cudaEventCreate(&timeA);
	cudaEventCreate(&timeB);
	cudaEventCreate(&timeC);
	cudaEventCreate(&timeD);
	
	CUDA_CHECK( cudaThreadSynchronize() );
	cudaEventRecord( timeA, 0 );
	
	CUDA_CHECK( cudaMalloc((void **)&FluxArray_flag_dev, size1) ); 
	CUDA_CHECK( cudaMalloc((void **)&FluxArray_dev, size2) );	
	CUDA_CHECK( cudaMalloc((void **)&particles_dev, size3) );
	CUDA_CHECK( cudaMalloc((void **)&DensArray_dev, size2) ); 
	CUDA_CHECK( cudaMalloc((void **)&x_HIArray_dev, size2) ); 
	
	CUDA_CHECK( cudaMemcpy(DensArray_dev, DensArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(x_HIArray_dev, x_HIArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(FluxArray_dev, FluxArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(FluxArray_flag_dev, FluxArray_flag, size1, cudaMemcpyHostToDevice) );
	
	cudaEventRecord( timeB, 0 );
	
	// Loop over the maximum number of particles
	for(int i=0; i<numLoops-1; i++)
	{	
		CUDA_CHECK( cudaMemcpy(particles_dev+i*MAX_PARTICLES, particles, size3, cudaMemcpyHostToDevice) );
	
		CUDA_CHECK( cudaThreadSynchronize() );

/*		if ((int)(step/5.)==step/5.)
		optically_thin<<<numBlocks1, numThreads1, size3>>>(DensArray_dev, x_HIArray_dev, FluxArray_dev, size, particles_dev, FluxArray_flag_dev);*/

		optically_thick<<<numBlocks1, numThreads1, size3f>>>(	DensArray_dev, x_HIArray_dev, FluxArray_dev, size,
									particles_dev, FluxArray_flag_dev, L, a);
//		printf("made it here!");
		CUDA_CHECK( cudaGetLastError() );
		CUDA_CHECK( cudaThreadSynchronize() );
	}
	
	// Final loop instance specifically for remainder
	CUDA_CHECK( cudaMemcpy(particles_dev+numLoops*MAX_PARTICLES, particles, remParts, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaThreadSynchronize() );
	optically_thick<<<numBlocks1, numThreads1, remPartsf>>>(	DensArray_dev, x_HIArray_dev, FluxArray_dev, size,
									particles_dev, FluxArray_flag_dev, L, a);
	CUDA_CHECK( cudaGetLastError() );
	CUDA_CHECK( cudaThreadSynchronize() );
	
	cudaEventRecord( timeC, 0 );
	
	CUDA_CHECK( cudaMemcpy(FluxArray_flag, FluxArray_flag_dev, size1, cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaMemcpy(FluxArray, FluxArray_dev, size2, cudaMemcpyDeviceToHost) );
	
	CUDA_CHECK( cudaFree(particles_dev) );
	CUDA_CHECK( cudaFree(FluxArray_dev) );
	CUDA_CHECK( cudaFree(DensArray_dev) );
	CUDA_CHECK( cudaFree(x_HIArray_dev) );
	CUDA_CHECK( cudaFree(FluxArray_flag_dev) );
	
	CUDA_CHECK( cudaThreadSynchronize() );
	cudaEventRecord( timeD, 0 );
	
	cudaEventElapsedTime( &time0, timeA, timeB );
	cudaEventElapsedTime( &time1, timeB, timeC );
	cudaEventElapsedTime( &time2, timeC, timeD );
	
	cudaEventDestroy(timeA);
	cudaEventDestroy(timeB);
	cudaEventDestroy(timeC);
	cudaEventDestroy(timeD);
	
	printf("\nPocessing %d=%d/%d elements", numThreads1, size3, sizeof(source));
	printf("\nMemcp to: %f (sec)\tKernel: %f (sec)\tMemcpy from: %f (sec)\n", time0/1000, time1/1000, time2/1000);
	CUDA_CHECK( cudaThreadExit() );
}

void ion(float* DensArray, float* x_HIArray, float* FluxArray, float* dt, float a)
{
	int elements = DIMX*DIMY*(DIMZ);
	size_t size2=elements*sizeof(float);
	cudaExtent size = make_cudaExtent(DIMX, DIMY, DIMZ);
	
	cudaEventCreate(&timeB);
	cudaEventCreate(&timeC);
	cudaEventRecord( timeB, 0 );
	
	// 2. Copy data to GPU on each node
	CUDA_CHECK( cudaMalloc((void **)&FluxArray_dev, size2) ); 
	CUDA_CHECK( cudaMalloc((void **)&DensArray_dev, size2) ); 
	CUDA_CHECK( cudaMalloc((void **)&x_HIArray_dev, size2) ); 

	CUDA_CHECK( cudaMemcpy(FluxArray_dev, FluxArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(DensArray_dev, DensArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(x_HIArray_dev, x_HIArray, size2, cudaMemcpyHostToDevice) );
	
	int maxdt=49999;
	CUDA_CHECK( cudaMalloc((void **)&dtnew, sizeof(int)) ); 	
	CUDA_CHECK( cudaMemcpy(dtnew, &maxdt, sizeof(int), cudaMemcpyHostToDevice) );
	
	ionization<<<numBlocks1, 1>>>(*dt, dtnew, DensArray_dev, x_HIArray_dev, FluxArray_dev, size, a); 
	
	CUDA_CHECK( cudaThreadSynchronize() );
	CUDA_CHECK( cudaMemcpy(&maxdt, dtnew, 1*sizeof(int), cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaFree(dtnew) );
	
//	*t+=*dt;
	float dt_new=(float)(maxdt)*1.e-5;
	//printf("dt= %f  dtnew=%f\n", dt, dt_new);
//	*dt=dt_new;

	cudaEventRecord( timeC, 0 );
	
	cudaEventElapsedTime( &time1, timeC, timeB );
//	printf("Processing time: %f (sec)\n", time0);
	
	CUDA_CHECK( cudaThreadSynchronize() );
	CUDA_CHECK( cudaMemcpy(DensArray, DensArray_dev, size2, cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaMemcpy(x_HIArray, x_HIArray_dev, size2, cudaMemcpyDeviceToHost) );
	
	CUDA_CHECK( cudaFree(FluxArray_dev) );
	CUDA_CHECK( cudaFree(DensArray_dev) );
	CUDA_CHECK( cudaFree(x_HIArray_dev) );
	
	CUDA_CHECK( cudaThreadExit() );
}
