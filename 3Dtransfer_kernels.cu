// Test
#include <cuda.h>
#include <math_functions.h>

// This shouldn't be here, but whatever //
typedef unsigned int uint;
typedef unsigned char uchar;

#define IMUL(a, b) __mul24(a, b)

#define MAX_EPSILON_ERROR	5.00f
#define THRESHOLD			0.30f
#define SCALE 				4.0

__device__ static float atomicMax(float* address, float val)
{
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}

// Original code:
/*	float rec=1.e-11;	// ~100 K
	float coll=0.0;	
	float x_e=1.0-xh1;
	float n_e=nh*x_e;
	float ff1=-(gam+coll*n_e);
	float ff2=rec*n_e*x_e;
	float ff=ff1*xh1+ff2;
	xh1+=ff*dt*3.15e13;	// s in a Myr
*/

__device__ void derivs(float x, float* y, float* dydx)
{
	float alphaHI = 4.30e-13;
	float alphaHeI = 4.31e-13;
	
	float ne = y[2]*(0.75*(1.0-y[0])+0.0625*(1.0-y[1]));
	
	dydx[0] = (-y[3]*y[0]+alphaHI*(1.0-y[0])*ne)*3.15e13;
	dydx[1] = (-y[4]*y[1]+alphaHeI*(1.0-y[1])*ne)*3.15e13;
	dydx[2] = dydx[3] = dydx[4] = 0;
	/*
	float ne = dens*(0.75*(1.0-y[0])+0.0625*(1.0-y[1]));
	
	dydx[0] = -gam[0]*y[0]+alphaHI*(1.0-y[0])*ne*3.15e13;
	dydx[1] = -gam[1]*y[1]+alphaHeI*(1.0-y[1])*ne*3.15e13;
	*/
}

__device__ void jacobn(const float x, float *y, float *dfdx, float *dfdy)
{
	float alphaHI = 4.30e-13;
	float alphaHeI = 4.31e-13;
	
	int n = 5;
	for (int i=0;i<n;i++) dfdx[i]=0.0;
	
	float ne	= y[2]*(0.75*(1.0-y[0])+0.0625*(1.0-y[1]));
	float dnd0	= -0.75*y[2];
	float dnd1	= -0.0625*y[2];
	
	dfdy[0] = (-y[3]+alphaHI*(-ne+(1.0-y[0])*dnd0))*3.15e13;
	dfdy[1] = (alphaHI*(1.0-y[0])*dnd1)*3.15e13;
	dfdy[5] = (alphaHeI*(1.0-y[1])*dnd0)*3.15e13;
	dfdy[6] = (-y[4]+alphaHeI*(-ne+(1.0-y[1])*dnd1))*3.15e13;
	
	for(int i=0;i<n;i++)
		for(int j=2;j<n;j++)
			dfdy[i*n+j]=0;
	for(int i=2;i<n;i++)
		for(int j=0;j<n;j++)
			dfdy[i*n+j]=0;
}

// This is here because derivs is inherently inline under CUDA architecture.
#include "./inc/rkck.cu"
#include "./inc/simpr.cu"

__device__ void test(float* y, float* dydx, float x, float h, float* yout, float* yerr, void derivs(float, float*, float*))
{
	int n = 5;
	for(int i=0;i<n;i++) {
		yout[i] = y[i] + h*dydx[i];
	}	
}

// Not sure this should be here/in this format, but both functions need the arrays
__device__ float sig[SPECIES][FREQ_BIN_NUM];
__device__ float gfn[FREQ_BIN_NUM] = {0.75, 0.25};
//__device__ float sig[FREQ_BIN_NUM] = {6.0e-18/1.0, 6.0e-18/8.0};
//__device__ float gfn[FREQ_BIN_NUM] = {0.5, 0.5};

__global__ void ionization(	float dt, float* error, float* density, float* x_N, float* FluxArray,
							cudaExtent size, float a)
{
    int dimx=(int)size.width;
    int dimy=(int)size.height;
    int dimz=(int)size.depth;
    int nelements=dimx*dimy*dimz;
	
    int i0 = blockIdx.x;
    int j0 = blockIdx.y;
    int k0 = blockIdx.z;

	int index=i0+dimx*j0+dimx*dimy*k0;
	if (index > nelements-1){
		index=nelements-1;
	}
		
	float xn[3+SPECIES];			// Neutral abundances 0<=x<=1
	xn[2] = density[index]/(a*a*a);	// Baryonic number density
	for(int nSpe=0; nSpe<SPECIES; nSpe++)
		xn[3+nSpe] = 0.0;			// Photon rates
	
	//////////////////////////////////////////
	sig[0][0] = 6.30e-18;	// 13.6 eV
	sig[0][1] = 1.24e-18;	// 24.6 eV
	sig[1][0] = 0.00e-18;	// 13.6 eV
	sig[1][1] = 7.81e-18;	// 24.6 eV
	//////////////////////////////////////////	
	for(int nSpe=0; nSpe<SPECIES; nSpe++)
	{
		xn[nSpe] = x_N[index+nSpe*nelements];
		for(int nBin=0; nBin<FREQ_BIN_NUM; nBin++)
			xn[3+nSpe] += gfn[nBin]*sig[nSpe][nBin]*FluxArray[index+nBin*nelements];
	}
	
/*void rkck(float* y, float* dydx, const float x,
	const float h, float* yout, float* yerr,
	void derivs(const float, float* , float* ))*/
/* void simpr(float* y, float* dydx, float* dfdx, float** dfdy,
	const float xs, const float htot, const int nstep, float* yout,
	void derivs(const float, float* , float*))*/
	float xerr[5];
	float dxdt[5], xn_out[5];
	float dfdt[5], dfdx[5*5];
	derivs(0, xn, dxdt);
	jacobn(0, xn, dfdt, dfdx);
//	rkck(xn, dxdt, 0, dt, xn_out, xerr, derivs);
	simpr(xn, dxdt, dfdt, dfdx, 0, dt, 2, xn_out, derivs);
	
	__syncthreads();
	for(int nSpe=0; nSpe<SPECIES; nSpe++)
	{
		if (xn_out[nSpe] < 0.0) {xn_out[nSpe]=0.0;}
		if (xn_out[nSpe] > 1.0) {xn_out[nSpe]=1.0;}
		
		x_N[index+nSpe*nelements] = xn_out[nSpe];
	}
	
	atomicMax(error, xerr[0]);
//	__syncthreads();
}

// Device code 
__global__ void optically_thick(float *DensArray, float *x_NArray, float *FluxArray, float *MetalArray,
                         cudaExtent size, source *sources, float L, float a) 
{

    extern __shared__ float acc_flux[];
    __shared__ float flux[FREQ_BIN_NUM];
    
    for(int nBin=0; nBin<FREQ_BIN_NUM; nBin++)
		flux[nBin] = 0.0;
	
	//////////////////////////////////////////
	sig[0][0] = 6.30e-18;	// 13.6 eV
	sig[0][1] = 1.24e-18;	// 24.6 eV
	sig[1][0] = 0.00e-18;	// 13.6 eV
	sig[1][1] = 7.81e-18;	// 24.6 eV
	//////////////////////////////////////////	
	
    int dimx=(int)size.width;
    int dimy=(int)size.height;
    int dimz=(int)size.depth;
    int nelements=dimx*dimy*dimz;
    
    float fDelta = L/DIMX;	// Length of ray tracing step in Mpc

    int partID=threadIdx.x;
	for(int nBin=0; nBin<FREQ_BIN_NUM; nBin++)
		acc_flux[partID+nBin*blockDim.x]=0.0;

    int i0 = blockIdx.x;
    int j0 = blockIdx.y;
    int k0 = blockIdx.z;

    int index=i0+dimx*j0+dimx*dimy*k0;
    if (index > nelements-1)
		index=nelements-1;
    
	// Find index of the point closest to the particle.
	int iX = (int) sources[partID].x;
	int iY = (int) sources[partID].y;
	int iZ = (int) sources[partID].z;
	int pI = iX + dimx*iY + dimx*dimy*iZ;

//Speedup trick
//    if (FluxArray_flag[index])
	// If BURST_PERIOD has passed, reset MetalArray to allow stars to form
	if(MetalArray[pI] >= BURST_PERIOD) MetalArray[pI] = 0;
    if(MetalArray[pI] <= BURST_LENGTH)
    {
		float dx=(sources[partID].x-i0);
		float dy=(sources[partID].y-j0);
		float dz=(sources[partID].z-k0);
		float phi=atan2f(dy,dx);
		float dxy2=dx*dx+dy*dy;
		float dxy=sqrtf(dxy2);
		float th=atanf(dz/dxy);

		float proj1=__cosf(th)*__cosf(phi);
		float proj2=__cosf(th)*__sinf(phi);
		float proj3=__sinf(th);

		float rmax2=dxy2+dz*dz;
		float rmax=sqrtf(rmax2);
		
		int thick[FREQ_BIN_NUM];
		float tau[FREQ_BIN_NUM];
		for(int sBin=0; sBin<FREQ_BIN_NUM; sBin++)
		{
			thick[sBin] = 0;
			tau[sBin] = 0.0;
		}
		if (rmax >1.414)
		{
			float r=0.0;
			int index_r,i=0,j=0,k=0;
			while (r<rmax && thick[0]==0 && thick[1]==0)
			{
				r=r+1.0;
				i=i0+(int)(r*proj1);
				j=j0+(int)(r*proj2);
				k=k0+(int)(r*proj3);
	
				if (k >=dimz-1) k=dimz-1; 
				if (j >=dimy-1) j=dimy-1; 
				if (i >=dimx-1) i=dimx-1;
				if (k <0) k=0; 
				if (j <0) j=0; 
				if (i <0) i=0;
				index_r=i+dimx*j+dimx*dimy*k;

				if (index_r > nelements-1) index_r=nelements-1;
			 
				if (index_r != index)
				{
					for(int sBin=0; sBin<FREQ_BIN_NUM; sBin++)
					{
						tau[sBin] += 0.75*(sig[0][sBin]*3.086e24*fDelta)*x_NArray[index_r];
						tau[sBin] += 0.0625*(sig[1][sBin]*3.086e24*fDelta)*x_NArray[index_r+nelements];
						tau[sBin] *= DensArray[index_r]/(a*a*a);
					}
				}
				for(int sBin=0; sBin<FREQ_BIN_NUM; sBin++)
					if (tau[sBin] > 3.0)
						thick[sBin]=1;
			}
		}
		for(int sBin=0; sBin<FREQ_BIN_NUM; sBin++)
		{
			if (thick[sBin]==0)
			{
				float damp=1.0;
				if (tau[sBin] < 0.5) damp=1.0-tau[sBin];
				else damp=__expf(-tau[sBin]);
				acc_flux[partID+sBin*blockDim.x]=__fdividef(sources[partID].gam,rmax2*L*L/dimx/dimx)*damp;
			}
		}
	}
	
	for(int nBin=0; nBin<FREQ_BIN_NUM; nBin++)
		atomicAdd(flux+nBin, acc_flux[partID+nBin*blockDim.x]);
	
	__syncthreads();
	for(int nBin=0; nBin<FREQ_BIN_NUM; nBin++)
		FluxArray[index+nBin*nelements]+=flux[nBin];
}
