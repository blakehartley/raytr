#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <iostream>
#include <fstream>
using namespace std;

#include "3Dtransfer.h"

typedef unsigned int uint;
typedef unsigned char uchar;

#define IMUL(a, b) __mul24(a, b)

#define MAX_EPSILON_ERROR 5.00f
#define THRESHOLD         0.30f

#define block_x 32

//double h=0.6711;
//double h0=h*3.246753e-18;
//double omegam=0.3;
//double yrtos=3.15569e7;
//time = time*yrtos*1.e6;
#define REDSHIFT_CONV (3.*0.6711*3.246753e-18*pow(0.3,0.5)/2.)*(3.15569e7*1.e6)

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "./3Dtransfer_kernels.cu"

// GLOBAL VARIABLES FOR DISPLAY
int numThreads1, numThreads2;
dim3 numBlocks1; 
long elements; 
__device__
float* DensArray_dev; 
__device__
float* x_NArray_dev;
__device__
float* FluxArray_dev; 
__device__
float* MetalArray_dev;

cudaExtent size;
__device__
source* particles_dev;

__device__
float* err_dev;

cudaEvent_t timeA, timeB, timeC, timeD;
float time0, time1, time2;

// Error handling macro
#define CUDA_CHECK(call) \
	if((call) != cudaSuccess) { \
		cudaError_t err = cudaGetLastError(); \
		cerr << "CUDA error calling \""#call"\", code is " << err << endl; \
		my_abort(err); }

// Perform calculations to produce Flux arrays
void rad(	float* DensArray, float* x_NArray, source* particles, float* FluxArray,
			float* MetalArray, int numParts, float L, float a)
{
	long elements = DIMX*DIMY*DIMZ;
	size_t size2=elements*sizeof(float);
	size = make_cudaExtent(DIMX, DIMY, DIMZ);
	
	numBlocks1.x=DIMX;
	numBlocks1.y=DIMY;
	numBlocks1.z=DIMZ;
	
	// Use numParts-1 because we always execute final loop.
	size_t numLoops		= (numParts-1)/MAX_SOURCES;
	
	numThreads1			= MAX_SOURCES;
	size_t maxParts		= numThreads1*sizeof(source);
	size_t sharedmem1	= numThreads1*sizeof(float)*FREQ_BIN_NUM;
	
	numThreads2			= (numParts-1)%MAX_SOURCES+1;
	size_t remParts		= numThreads2*sizeof(source);
	size_t sharedmem2	= numThreads2*sizeof(float)*FREQ_BIN_NUM;
	
	cudaEventCreate(&timeA);
	cudaEventCreate(&timeB);
	cudaEventCreate(&timeC);
	cudaEventCreate(&timeD);
	
	CUDA_CHECK( cudaThreadSynchronize() );
	cudaEventRecord( timeA, 0 );
	
	CUDA_CHECK( cudaMalloc((void **)&x_NArray_dev, size2*SPECIES) );
	CUDA_CHECK( cudaMalloc((void **)&FluxArray_dev, size2*FREQ_BIN_NUM) );	
	CUDA_CHECK( cudaMalloc((void **)&DensArray_dev, size2) ); 
	CUDA_CHECK( cudaMalloc((void **)&MetalArray_dev, size2) );
	
	CUDA_CHECK( cudaMemcpy(x_NArray_dev, x_NArray, size2*SPECIES, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(FluxArray_dev, FluxArray, size2*FREQ_BIN_NUM, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(DensArray_dev, DensArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(MetalArray_dev, MetalArray, size2, cudaMemcpyHostToDevice) );
	
	cudaEventRecord( timeB, 0 );
	
	// Loop over the maximum number of particles
	for(int i=0; i<numLoops; i++)
	{
		CUDA_CHECK( cudaMalloc((void **)&particles_dev, maxParts) );
		CUDA_CHECK( cudaMemcpy(particles_dev, particles+i*MAX_SOURCES, maxParts, cudaMemcpyHostToDevice) );
		CUDA_CHECK( cudaThreadSynchronize() );

/*		if ((int)(step/5.)==step/5.)
		optically_thin<<<numBlocks1, numThreads1, size3>>>(DensArray_dev, x_NArray_dev, FluxArray_dev, size, particles_dev, FluxArray_flag_dev);*/
		
		CUDA_CHECK( cudaGetLastError() );
		optically_thick<<<numBlocks1, numThreads1, sharedmem1>>>(	DensArray_dev, x_NArray_dev, FluxArray_dev, MetalArray_dev,
																	size, particles_dev, L, a);
		CUDA_CHECK( cudaGetLastError() );
		
		CUDA_CHECK( cudaThreadSynchronize() );
		CUDA_CHECK( cudaFree(particles_dev) );
	}
	
	// Final loop instance specifically for remainder
	CUDA_CHECK( cudaMalloc((void **)&particles_dev, remParts) );
	CUDA_CHECK( cudaMemcpy(particles_dev, particles+numLoops*MAX_SOURCES, remParts, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaThreadSynchronize() );
	
	CUDA_CHECK( cudaGetLastError() );
	optically_thick<<<numBlocks1, numThreads2, sharedmem2>>>(	DensArray_dev, x_NArray_dev, FluxArray_dev, MetalArray_dev,
																size, particles_dev, L, a);
	CUDA_CHECK( cudaPeekAtLastError() );
	CUDA_CHECK( cudaThreadSynchronize() );
	CUDA_CHECK( cudaGetLastError() );
	
	cudaEventRecord( timeC, 0 );
	
	CUDA_CHECK( cudaMemcpy(FluxArray, FluxArray_dev, size2*FREQ_BIN_NUM, cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaMemcpy(MetalArray, MetalArray_dev, size2, cudaMemcpyDeviceToHost) );
	
	CUDA_CHECK( cudaFree(particles_dev) );
	CUDA_CHECK( cudaFree(DensArray_dev) );
	CUDA_CHECK( cudaFree(x_NArray_dev) );
	CUDA_CHECK( cudaFree(FluxArray_dev) );
	CUDA_CHECK( cudaFree(MetalArray_dev) );
	
	CUDA_CHECK( cudaThreadSynchronize() );
	cudaEventRecord( timeD, 0 );
	
	cudaEventElapsedTime( &time0, timeA, timeB );
	cudaEventElapsedTime( &time1, timeB, timeC );
	cudaEventElapsedTime( &time2, timeC, timeD );
	
	cudaEventDestroy(timeA);
	cudaEventDestroy(timeB);
	cudaEventDestroy(timeC);
	cudaEventDestroy(timeD);
	
//	printf("\nPocessing %d=%d/%d elements", numThreads1, remParts, sizeof(source));
	printf("\nMemcp to: %f (sec)\tKernel: %f (sec)\tMemcpy from: %f (sec)\n", time0/1000, time1/1000, time2/1000);
	CUDA_CHECK( cudaThreadExit() );
}

void ion(float* DensArray, float* x_NArray, float* FluxArray, float* dt, float* fErr, float a)
{
	*fErr = 0.0;
	long elements = DIMX*DIMY*DIMZ;
	size_t size2=elements*sizeof(float);
	cudaExtent size = make_cudaExtent(DIMX, DIMY, DIMZ);
	
	cudaEventCreate(&timeB);
	cudaEventCreate(&timeC);
	cudaEventRecord( timeB, 0 );
	
	// 2. Copy data to GPU on each node
	CUDA_CHECK( cudaMalloc((void **)&FluxArray_dev, size2*FREQ_BIN_NUM) ); 
	CUDA_CHECK( cudaMalloc((void **)&DensArray_dev, size2) ); 
	CUDA_CHECK( cudaMalloc((void **)&x_NArray_dev, size2*SPECIES) ); 

	CUDA_CHECK( cudaMemcpy(FluxArray_dev, FluxArray, size2*FREQ_BIN_NUM, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(DensArray_dev, DensArray, size2, cudaMemcpyHostToDevice) );
	CUDA_CHECK( cudaMemcpy(x_NArray_dev, x_NArray, size2*SPECIES, cudaMemcpyHostToDevice) );
	
	CUDA_CHECK( cudaMalloc((void **)&err_dev, sizeof(float)) ); 	
	CUDA_CHECK( cudaMemcpy(err_dev, fErr, sizeof(float), cudaMemcpyHostToDevice) );
	
	ionization<<<numBlocks1, 1>>>(*dt, err_dev, DensArray_dev, x_NArray_dev, FluxArray_dev, size, a); 
	
	CUDA_CHECK( cudaThreadSynchronize() );
	CUDA_CHECK( cudaMemcpy(fErr, err_dev, sizeof(float), cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaFree(err_dev) );

	cudaEventRecord( timeC, 0 );
	
	cudaEventElapsedTime( &time1, timeC, timeB );
//	printf("Processing time: %f (sec)\n", time0);
	
	CUDA_CHECK( cudaThreadSynchronize() );
	
	CUDA_CHECK( cudaMemcpy(FluxArray, FluxArray_dev, size2*FREQ_BIN_NUM, cudaMemcpyDeviceToHost) );
	
	CUDA_CHECK( cudaMemcpy(DensArray, DensArray_dev, size2, cudaMemcpyDeviceToHost) );
	CUDA_CHECK( cudaMemcpy(x_NArray, x_NArray_dev, size2*SPECIES, cudaMemcpyDeviceToHost) );
	
	CUDA_CHECK( cudaFree(FluxArray_dev) );
	CUDA_CHECK( cudaFree(DensArray_dev) );
	CUDA_CHECK( cudaFree(x_NArray_dev) );
	
	CUDA_CHECK( cudaThreadExit() );
}
