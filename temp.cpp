#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char** argv)
{
	// Load source file scale factors
	ifstream file("./nanoJubilee/outputs_a.txt");
	float nfA[96];	
	for(int i=0; i<96; i++)
	{
		file >> nfA[i];
//		printf("%1.4f, ", round(1.e4*nfA[i])*1.e-4 );
	}
	file.close();
	
	// This is the primary loop.
	int nFileNum=0, nHaloCount;
	
	//	Read particle file if necessary		//
	for(int i=0; i<96; i++)
	{
		nHaloCount=0;
		char name[50];
		sprintf(name, "./nanoJubilee/D178c_VTUNE=1_a%1.4f.AHF_halos", round(1.e4*nfA[i])*1.e-4);
//		cout << name << endl;
		string line;
		
		ifstream file(name);
		while(getline(file,line))
		{
//			cout << line << endl;
			nHaloCount++;
		}
		file.close();
		
		cout << nHaloCount-1 << endl;
	}
	return 0;
}
