#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <algorithm>

#include <iostream>
#include <fstream>
using namespace std;

#include <mpi.h>

#include "3Dtransfer.h"

// Global variables
#define SCALE 4.0
#define sigma_eff_dx 1.0/SCALE
#define BOX_SIZE 10.0	// Length of DIMX side in Mpc
#define HUBBLE 0.7		// h

// Error handling macros
#define MPI_CHECK(call) \
	if((call) != MPI_SUCCESS) { \
		cerr << "MPI error calling \""#call"\"\n"; \
		my_abort(-1); }
		
void halomatch(float*, unsigned, float, float, int);
double schechter(double,double, double, double);
double model_Mstar(double, unsigned);
double model_log10phi(double, unsigned);
double model_alpha(double, unsigned);

double time(double redshift){
	double h=0.6711;
	double h0=h*3.246753e-18;
	double omegam=0.3;
	double yrtos=3.15569e7;
	double time = 2.*pow((1.+redshift),-3./2.)/(3.*h0*pow(omegam,0.5));
	time = time/(yrtos*1.e6);
	return time;
}

double redshift(double time){
	double h=0.6711;
	double h0=h*3.246753e-18;
	double omegam=0.3;
	double yrtos=3.15569e7;
	time = time*yrtos*1.e6;
	double redshift = pow((3.*h0*pow(omegam,0.5)*time/2.),-2./3.)-1.;
	return redshift;
}

void output(float* FluxArray, float* x_NArray, long elements, int filenum)
{
	char sFile0[32];
	char sFile1[32];
	char sFile2[32];
	sprintf(sFile0, "./dat/xhi%d.bin", filenum);
	sprintf(sFile1, "./dat/xhei%d.bin", filenum);
	sprintf(sFile2, "./dat/flux%d.bin", filenum);
	ofstream ofsData0 (sFile0, ios::out | ios::app | ios::binary);
	ofstream ofsData1 (sFile1, ios::out | ios::app | ios::binary);
	ofstream ofsData2 (sFile2, ios::out | ios::app | ios::binary);
	
	int mod = DIMX/128;
	cout << "mod = " << mod << endl;
	for(long i=0; i<elements; i++)
	{
		int i0 = i%(DIMX);
		int j0 = i/(DIMX);
		int k0 = i/(DIMX*DIMY);
		if(i0%mod==0 && j0%mod==0 && k0%mod==0)
		{
			ofsData0.write((char*)(x_NArray+i), sizeof(float));
			ofsData1.write((char*)(x_NArray+elements+i), sizeof(float));
			ofsData2.write((char*)(FluxArray+i), sizeof(float));
			ofsData2.write((char*)(FluxArray+elements+i), sizeof(float));
		}
	}

	ofsData0.close();
	ofsData1.close();
	ofsData2.close();
}
void print_ne(float* DensArray, float* x_NArray, float scale, long elements)
{
	char sFile[32];
	sprintf(sFile, "./dat/ne.bin");
	ofstream ofsData (sFile, ios::out | ios::app | ios::binary);
	float ne = 0, Yp = 0.25;
	
	for(long i=0; i<elements; i++)
	{
		ne +=	DensArray[i]*((1.0-Yp)*(1.0-x_NArray[i])+0.25*Yp*(1.0-x_NArray[elements+i]))*pow(scale,-3.0);
	}
	
	ne = ne/(DIMX*DIMY*DIMZ);
	
	ofsData.write((char*)(&ne), sizeof(float));
	ofsData.close();
}

inline float frand()
{
    return rand() / (float) RAND_MAX;
}

bool sortByGam(const source &lhs, const source &rhs) {return lhs.gam > rhs.gam;}

int main(int argc, char** argv)
{
	if(argc != 4)
	{
		cout << "Format: dt N mod" << endl;
		return -1;
	}
	float fDt		= atof(argv[1]);
	int dLoopNum 	= atoi(argv[2]);
	int dFileMod 	= atoi(argv[3]);
	
	// Variables available on every node at all times
	long elements = DIMX*DIMY*DIMZ;
	size_t size1=elements*sizeof(short);
	size_t size2=elements*sizeof(float);
	size_t size3=MAX_SOURCES*sizeof(source);
	
    // Initialize MPI state
    MPI_CHECK(MPI_Init(&argc, &argv));

    // Get our MPI node number and node count
    int commSize, commRank;
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &commSize));
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &commRank));
	
	// Create a type for our source struct
	const int 		nitems= 4;
	int				blocklengths[4] = {1,1,1,1};
	MPI_Datatype	types[4] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
	MPI_Datatype	mpi_source;
	MPI_Aint		offsets[4];
	
	offsets[0]		= offsetof(source, x);
	offsets[1]		= offsetof(source, y);
	offsets[2]		= offsetof(source, z);
	offsets[3]		= offsetof(source, gam);
	
	MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_source);
	MPI_Type_commit(&mpi_source);
	
	// Allocate CPU arrays on all nodes
	float *DensArray = (float *)malloc(size2);
	float *x_NArray = (float *)malloc(size2*SPECIES);
	float *FluxArray = (float *)malloc(size2*FREQ_BIN_NUM);
	float *MetalArray = (float *)malloc(size2);
		
	float *FluxArray_red = (float *)malloc(size2*FREQ_BIN_NUM);
	float *MetalArray_red = (float *)malloc(size2);
	
	source *particles_loc;			
	source *particles;
	
	float fL = 3.0857e19;
	
	double dTStart,dT0,dT1,dT2;
	if(commRank==0) dTStart=MPI_Wtime();
	
	// Initialize arrays on the root node
	if (commRank == 0)
	{
		cout << "Running on " << commSize << " nodes" << endl;
		
		// Grid properties written here
		srand(2110);
		float *ptr2 = DensArray;
		float *ptr3 = x_NArray;
		float *ptr5 = MetalArray;
		
		for(long i=0; i<elements; i++)
		{
			//density
			*ptr2++ = 2.43e-7;	// cm^-3
			// Time since last burst in grid point
			*ptr5++ = BURST_PERIOD + 1.0;
		}
		for(long i=0; i<SPECIES*elements; i++)
			*ptr3++ = 1.0;	//neutral fraction
	}
	
	// Send data to all nodes.
	MPI_CHECK(MPI_Bcast(FluxArray, elements*FREQ_BIN_NUM, MPI_FLOAT, 0, MPI_COMM_WORLD));
	MPI_CHECK(MPI_Bcast(DensArray, elements, MPI_FLOAT, 0, MPI_COMM_WORLD));
	MPI_CHECK(MPI_Bcast(x_NArray, elements*SPECIES, MPI_FLOAT, 0, MPI_COMM_WORLD));	
	MPI_CHECK(MPI_Bcast(MetalArray, elements, MPI_FLOAT, 0, MPI_COMM_WORLD));
	
	// Load source file scale factors
	ifstream scalefactors;
	scalefactors.open("../nanoJubilee/outputs_a.txt");
	float nfA[94];	
	for(int i=0; i<94; i++)
	{
		scalefactors >> nfA[i];
	}
	scalefactors.close();
	
	ifstream halocounts;
	halocounts.open("../nanoJubilee/counts.txt");
	int niHaloCount[94], niHaloLimit[94];	
	for(int i=0; i<94; i++)
	{
		halocounts >> niHaloCount[i];
		niHaloLimit[i] = ((niHaloCount[i]-1)/commSize+1)*commSize;
//		cout << niHaloLimit[i] << endl;
	}
	halocounts.close();
	
	// This is the primary loop.
	int counter=0, iFileNum=0;
//	float fT=time(30-0.01);
	float fT=time(30.0-0.01);
	
//	printf("Thread #%f made it here!\n", commRank);
	for(int step=0; step < dLoopNum; step++)
	{
		//	Read particle file if necessary		//
		if( fT > time(1.0/nfA[iFileNum]-1.0))
		{
			particles_loc = new source[niHaloLimit[iFileNum]/commSize];			
			particles = new source[niHaloLimit[iFileNum]];
			
			float* mags = new float[niHaloCount[iFileNum]];
			halomatch(mags, niHaloCount[iFileNum], nfA[iFileNum], pow(BOX_SIZE,3), 0);
			
			char name[50];
			sprintf(name, "../nanoJubilee/D178c_VTUNE=1_a%1.4f.AHF_halos", round(1.e4*nfA[iFileNum])*1.e-4);
			string line;
			ifstream file(name);
			getline(file, line);	// Header
			
			if(commRank == 0)
			{
				for (uint i=0; i<niHaloCount[iFileNum]; i++)
				{
					float *nfData = new float[43];
	
					for(int j=0; j<43; j++)
					{
						file >> nfData[j];
//						printf("%f\n", nfData[j]);
					}
	
					particles[i].x=(nfData[5]/(1.e4*HUBBLE))*DIMX;
					particles[i].y=(nfData[6]/(1.e4*HUBBLE))*DIMY;
					particles[i].z=(nfData[7]/(1.e4*HUBBLE))*DIMZ;
//					particles[i].w=nfData[3];	// Mass in solar masses.
					particles[i].gam=nfData[3];	// Mass in solar masses, stored in gam slot
					
					delete[] nfData;
				}
				
				sort(particles, particles + niHaloCount[iFileNum], sortByGam);
				
				for (uint i=0; i<niHaloCount[iFileNum]; i++)
				{// from 2e25 formula from Kuhlen
					particles[i].gam = 9.426e6*exp(0.4*2.30259*(-25.0-mags[i]));
				}
				
				delete[] mags;
				
				for(uint i=niHaloCount[iFileNum]; i<niHaloLimit[iFileNum]; i++)
				{
					particles[i].x=-1;
					particles[i].y=-1;
					particles[i].z=-1;
					particles[i].gam=0;
				}
			}
			
			file.close();
			
			MPI_CHECK(	MPI_Scatter(	particles, niHaloLimit[iFileNum]/commSize, mpi_source,
										particles_loc, niHaloLimit[iFileNum]/commSize, mpi_source,
										0, MPI_COMM_WORLD));
			delete[] particles;
			iFileNum++;
			if(commRank == 0) {printf("T = %f Myr. Currently tracking %d halos.\n", fT, niHaloLimit[iFileNum-1]);}
		}
		
/*		if(step%10 == 0)
		{
			printf("Node %d has entries:", commRank);
			for(int i=0; i<niHaloLimit[iFileNum-1]/commSize; i++)
			{
				printf("%e ", particles_loc[i].gam);
			}
			printf("\n");
		}*/
		//	rad step: computer the Flux array	//
		// 1. Allocate on all nodes and Bcast/Scatter arrays:
		MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
		if(commRank==0) {dT0=MPI_Wtime(); printf("0...");}
		
		// 2.-5. Done by rad, in 3Dtransfer.cu. Begin by making FluxArray = 0
		for(long i=0; i<elements*FREQ_BIN_NUM; i++)	FluxArray[i] = 0;
		
		rad(	DensArray, x_NArray, particles_loc, FluxArray, MetalArray,
				niHaloLimit[iFileNum-1]/commSize, BOX_SIZE, nfA[iFileNum-1]);
		
		// 6. Reduce Flux Arrays
		MPI_CHECK(MPI_Allreduce(	FluxArray, FluxArray_red, elements*FREQ_BIN_NUM,
									MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD));
		MPI_CHECK(MPI_Allreduce(	MetalArray, MetalArray_red, elements,
									MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD));
		
		MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
		if(commRank==0) {dT1=MPI_Wtime(); printf("1...");}
		
		float temp = 0;
		ion(DensArray, x_NArray, FluxArray_red, &fDt, &temp, nfA[iFileNum-1]);
		
		if(commRank==0) {printf("Max RKCK error = %e\n", temp);}
		
		MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
		if(commRank==0) {dT2=MPI_Wtime(); printf("2...\n");}
		
		//	output as necessary					//
		if( step%dFileMod == 0 && commRank == 0)
		{
			output(FluxArray_red, x_NArray, elements,  step/dFileMod);
			print_ne(DensArray, x_NArray, nfA[iFileNum-1], elements);
			counter++;
		}
		
		MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));
		if(commRank==0) printf("Step #%d, rad step: %f, ion step: %f\n", step, dT1-dT0, dT2-dT1);
			
		for(long i=0; i<elements; i++)
		{
			if(MetalArray_red[i] <= BURST_PERIOD+2*fDt) MetalArray[i] = MetalArray_red[i] + fDt;
		}
		
		fT+=fDt;
		
		if( fT > time(1.0/nfA[iFileNum]-1.0)) delete[] particles_loc;
	}
	
	// Free host memory
	free(DensArray);
	free(FluxArray);
	free(x_NArray);
		
	free(FluxArray_red);

	MPI_CHECK(MPI_Finalize());
	
	if(commRank == 0) printf("Total run time: %f\n", MPI_Wtime()-dTStart);
	
	return 0;
}

// Shut down MPI cleanly if something goes wrong
void my_abort(int err)
{
    cout << "Test FAILED\n";
    MPI_Abort(MPI_COMM_WORLD, err);
}

// Returns magnitude of Nth halo
void halomatch(float* L, unsigned n, float a, float V, int mod)
{
	float z = 1.0/a-1.0;
	float lmin = 0, lmax = -25, dl = 0.01;
	float nschechter = 0;
	float ll = lmax;
	
	for(unsigned i=0; i<n; i++)
	{
		float dndl = schechter(ll, model_Mstar(z, mod), model_log10phi(z, mod), model_alpha(z, mod));
		while(nschechter + dl*dndl*10 > (i+1)/V && nschechter < (i+1)/V)
		{
//			cout << "Decreasing step size" << dl << " -> " << dl/10 << endl;
			dl = dl/10;
		}
		while(nschechter < (i+1)/V)
		{
			ll += dl;
			dndl = schechter(ll, model_Mstar(z, mod), model_log10phi(z, mod), model_alpha(z, mod))*BURST_PERIOD/BURST_LENGTH;
			nschechter += dl*dndl;
		}
		
//		cout << "N = " << nschechter << "\tM = " << ll << endl;
		L[i] = ll;
	}
}

double schechter(	double M,			// Mass
					double Mstar,		// Shechter parameters 
					double log10phi,		// "
					double alpha)		// "
{
	double temp = pow(10.,0.4*(Mstar-M));
	return 0.921034*pow(10.,log10phi)*pow(temp,alpha+1.)*pow(2.718281828,-temp);
}

double model_Mstar(double z, unsigned mod)
{
	if(mod == 1)	{return -20.37 + 0.30*(z - 6.);}
	else if(mod == 2)	{return -20.47 + 0.24*(z - 6.);}
	return -20.42 + 0.27*(z - 6.);
}

double model_log10phi(double z, unsigned mod)
{
	if(mod == 1)	{return -3.05 - 0.09*(z - 6.);}
	else if(mod == 2)	{return -2.97 - 0.05*(z - 6.);}
	return -3.01 - 0.07*(z - 6.);
}

double model_alpha(double z, unsigned mod)
{
	if(mod == 1)	{return -1.80 - 0.04*(z - 6.);}
	else if(mod == 2)	{return -1.88 - 0.08*(z - 6.);}
	return -1.84 - 0.06*(z - 6.);
}
